import { connect } from 'react-redux';

import UIPack from '../../../lib/uiPack.js';

const { Notification } = UIPack;

const mapStateToProps = (state) => {
  const {
    visible,
    message,
    type,
  } = state.notification;

  return {
    visible,
    message,
    type,
  };
};

export default connect(mapStateToProps)(Notification);
