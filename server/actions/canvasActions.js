import { Meteor } from 'meteor/meteor';
import SymbolicImage from '../lib/symbolicImage/symbolicImage';

class CanvasActions {
  getSymbolicImage (args) {
    return new Promise((resolve, reject) => {
      const symbolicImage = new SymbolicImage(
        args.gridX,
        args.gridY,
        args.gridW,
        args.gridH,
        args.gridD,
        args.dotNum,
        args.image,
      );

      symbolicImage.calcImage();
      symbolicImage.excludeNonReturnableItems();

      resolve(symbolicImage);
    });
  }
}

const canvasAction = new CanvasActions();

Meteor.methods({
  'getSymbolicImage': (args) => canvasAction.getSymbolicImage(args),
});

export default canvasAction;
