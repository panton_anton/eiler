import React, { Component } from 'react';

class Dashboard extends Component {
  constructor (props) {
    super(props);

    this.state = {
      page: 'iter',
      startX: 0,
      endX: 1000,
      step: 0.01,
      scale: 300,
      iterNumber: 0,
      iterX: 'y',
      iterY: '-0.25*y + 0.5*x*(1-x*x) + 0.5*cos(0.2*t)',
      x0: 0,
      y0: 0,
      dotNumber: 10,
      eilerFunc: 'x',
      iterStep: 0.1,
      symFunX: 'x',
      symFunY: 'y',
      symStartX: '-1.5',
      symStartY: '-1',
      symGridWidth: 30,
      symGridHeight: 20,
      symGridDelta: 0.1,
      clear: true,
    };
  }

  changeState (e) {
    const field = e.target.id;
    const value = e.target.value;
    console.log(field, value);

    this.setState((state) => ({
      [field]: value,
    }));
  }


  render () {
    const iterContent = (
      <div className="dashboard__content">
        <div className="field">
          <label>
            Start X:
          </label>
          <input
            id="startX"
            value={this.state.startX}
            onChange={this.changeState.bind(this)}
          />
        </div>
        <div className="field">
          <label>
            End X:
          </label>
          <input
            id="endX"
            value={this.state.endX}
            onChange={this.changeState.bind(this)}
          />
        </div>
        <div className="field">
          <label>
            Step:
          </label>
          <input
            id="step"
            value={this.state.step}
            onChange={this.changeState.bind(this)}
          />
        </div>
        <div className="field">
          <label>
            Iter number:
          </label>
          <input
            id="iterNumber"
            value={this.state.iterNumber}
            onChange={this.changeState.bind(this)}
          />
        </div>
        <div className="field">
          <label>
            Scale:
          </label>
          <input
            id="scale"
            value={this.state.scale}
            onChange={this.changeState.bind(this)}
          />
        </div>
        <div>
          <label>
            Iter Functions:
          </label>
          <input
            id="iterX"
            value={this.state.iterX}
            placeholder="x"
            onChange={this.changeState.bind(this)}
          />
          <input
            id="iterY"
            value={this.state.iterY}
            placeholder="y"
            onChange={this.changeState.bind(this)}
          />
        </div>
        <div className="field">
          <label>
            x0:
          </label>
          <input
            id="x0"
            value={this.state.x0}
            onChange={this.changeState.bind(this)}
          />
        </div>
        <div className="field">
          <label>
            y0:
          </label>
          <input
            id="y0"
            value={this.state.y0}
            onChange={this.changeState.bind(this)}
          />
        </div>
        <label>
          Clear canvas
        </label>
        <input
          type="checkbox"
          checked={this.state.clear}
          onChange={() => this.setState((state) => ({ clear: !state.clear }))}
        />
        <input
          type="button"
          value="Iterations"
          onClick={() => this.props.acceptIteration(this.state)}
        />
        <input
          type="button"
          value="Eiler"
          onClick={() => this.props.acceptEiler(this.state)}
        />
        <input
          type="button"
          value="Runge-Kutta"
          onClick={() => this.props.acceptRk(this.state)}
        />
      </div>
    );

    const symbolContent = (
      <div className="dashboard__content">
        <div>
          <label>
            Functions:
          </label>
          <input
            id="symFunX"
            value={this.state.symFunX}
            placeholder="x"
            onChange={this.changeState.bind(this)}
          />
          <input
            id="symFunY"
            value={this.state.symFunY}
            placeholder="y"
            onChange={this.changeState.bind(this)}
          />
        </div>
        <div className="field">
          <label>
            Start X:
          </label>
          <input
            id="symStartX"
            value={this.state.symStartX}
            onChange={this.changeState.bind(this)}
          />
        </div>
        <div className="field">
          <label>
            Start Y:
          </label>
          <input
            id="symStartY"
            value={this.state.symStartY}
            onChange={this.changeState.bind(this)}
          />
        </div>
        <div className="field">
          <label>
            Grid width:
          </label>
          <input
            id="symGridWidth"
            value={this.state.symGridWidth}
            onChange={this.changeState.bind(this)}
          />
        </div>
        <div className="field">
          <label>
            Grid height:
          </label>
          <input
            id="symGridHeight"
            value={this.state.symGridHeight}
            onChange={this.changeState.bind(this)}
          />
        </div>
        <div className="field">
          <label>
            Grid delta:
          </label>
          <input
            id="symGridDelta"
            value={this.state.symGridDelta}
            onChange={this.changeState.bind(this)}
          />
        </div>
        <div className="field">
          <label>
            Dot number:
          </label>
          <input
            id="dotNumber"
            value={this.state.dotNumber}
            onChange={this.changeState.bind(this)}
          />
        </div>
        <div className="field">
          <label>
            Scale:
          </label>
          <input
            id="scale"
            value={this.state.scale}
            onChange={this.changeState.bind(this)}
          />
        </div>
        <label>
          Clear canvas
        </label>
        <input
          type="checkbox"
          checked={this.state.clear}
          onChange={() => this.setState((state) => ({ clear: !state.clear }))}
        />
        <input
          type="button"
          value="Symbolic Image"
          onClick={() => this.props.acceptSymbolicImage(this.state)}
        />
      </div>
    );

    return (
      <div className="dashboard">
        <input
          type="button"
          value="ITERATIONS"
          onClick={() => this.setState(() => ({ page: 'iter' }))}
        />
        <input
          type="button"
          value="SYMBOLIC IMAGES"
          onClick={() => this.setState(() => ({ page: 'symbol' }))}
        />
        {
          this.state.page === 'symbol' ?
            symbolContent :
            this.state.page === 'iter' ?
              iterContent :
              null
        }
        {/* <div className="field">
          <label>
            Iter step:
          </label>
          <input
            id="iterStep"
            value={this.state.iterStep}
            onChange={this.changeState.bind(this)}
          />
        </div> */}
      </div>
    );
  }
}

export default Dashboard;
