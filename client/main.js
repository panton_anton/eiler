import { Meteor } from 'meteor/meteor';
import React from 'react';
import { render } from 'react-dom';

import App from './components/main';

// import Graph from './lib/graph';
//
// const g = new Graph(8);
// g.applyRelationsMatrix([
//   [0,0,0,0,0,0,0,0,],
//   [0,0,0,0,1,1,0,1,],
//   [0,1,0,1,0,0,0,0,],
//   [1,1,1,0,0,1,0,0,],
//   [0,0,1,0,0,0,0,0,],
//   [0,1,0,0,0,0,0,0,],
//   [0,0,0,0,0,0,0,0,],
//   [0,0,1,0,1,0,0,0,],
// ]);
// g._findSCCIter();
// console.log(g.components);

Meteor.startup(() => {
  const rootDiv = document.createElement('div');
  rootDiv.id = 'root';
  document.body.appendChild(rootDiv);
  render(<App />, document.getElementById('root'));

});
