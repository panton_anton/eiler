import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

import notification from '../reducers/notification';

const rootReducer = combineReducers({
  notification,
});

export default createStore(
  rootReducer,
  applyMiddleware(
    thunk,
    logger
  )
);
