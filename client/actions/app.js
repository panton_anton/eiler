import notification from './notification';

const config = {
  actionTypes: {
    SUCCESS_ITERATION: 'SUCCESS_ITERATION',
  },
};

class AppActions {
  constructor (config) {
    this.actionTypes = config.actionTypes;
  }

  acceptIterations (data) {
    return (dispatch) => {
      dispatch(notification.loading());

      Meteor.call('CanvasActions.getIterations', data, (err, res) => {
        if (err) {
          dispatch(notification.alert(err.message || err.reason));
          return;
        }
        dispatch(notification.hide());
        dispatch(this.successIteration(res));
      });
    }
  }

  successIteration (data) {
    return { type: this.actionTypes.SUCCESS_ITERATION, data }
  }
}

const appActions = new AppActions(config);

export default appActions;
